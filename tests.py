
import sys
import os
import unittest
from models import app, db, Restaurant, Cuisine, Drink_Place
from create_db import app, db, Restaurant, Drink_Place, Cuisine, create_restaurant

class DBTestCases(unittest.TestCase):
    app.config['TESTING'] = True
    #app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:group1@localhost:5432/businessdb'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:group1@localhost:5432/businessdb'
    app.app_context().push()

    # def tearDown(self):
    #     db.session.remove()
    #     db.drop_all()

    # checking to see if db inserts new object
    def test_restaurant_insert(self):
        r = Restaurant(
            business_id='1',
            res_name='Test Restaurant',
            phone='1234567890',
            latitude=45.0,
            longitude=45.0,
            address='Test Address',
            rating='4.5',
            hours=None,
            desc='Test Description',
            reveiws='Test Review',
            photos=None,
            cuisine=None,
            price_level='$$',
            delivery=True
        )

        db.session.add(r)
        db.session.commit()

        result = db.session.query(Restaurant).filter_by(res_name = 'Test Restaurant').one()
        self.assertEqual(result.res_name, 'Test Restaurant')
        
        db.session.query(Restaurant).filter_by(res_name = 'Test Restaurant').delete()
        db.session.commit()

    # checking to see if db updates
    def test_cuisine_update(self):
        c = Cuisine(
            id=128744,
            name='Test Cuisine',
            type='Test Type'
        )
        db.session.add(c)
        db.session.commit()

        result = db.session.query(Cuisine).filter_by(id=128744).one()
        result.name = 'Cuisine 0'
        self.assertEqual(result.name, 'Cuisine 0')

        db.session.query(Cuisine).filter_by(id=128744).delete()
        db.session.commit()

    # checking the drink place insert
    def test_drink_place_insert(self):
        d = Drink_Place(
            business_id='1936',
            drink_name='Test Drink Place',
            phone='1234567890',
            latitude=45.0,
            longitude=45.0,
            address='Test Address',
            rating='4.5',
            hours=None,
            desc='Test Description',
            reveiws='Test Review',
            photos=None,
            cuisine=None,
            price_level='2',
            delivery=True
        )
        db.session.add(d)
        db.session.commit()

        result = db.session.query(Drink_Place).filter_by(drink_name='Test Drink Place').one()
        self.assertEqual(result.drink_name, 'Test Drink Place')

        db.session.query(Drink_Place).filter_by(drink_name='Test Drink Place').delete()
        db.session.commit()

    # making sure everything was added correctly
    def test_restaurant_length(self):
        len_rest = db.session.query(Restaurant).count()
        self.assertEqual(len_rest, 126)

    #making sure everything was added correctly
    def test_cuisine_length(self):
        len_rest = db.session.query(Cuisine).count()
        self.assertEqual(len_rest, 144)

    def test_drink_length(self):
        len_rest = db.session.query(Drink_Place).count()
        self.assertEqual(len_rest, 42)    

if __name__ == '__main__':
    unittest.main()