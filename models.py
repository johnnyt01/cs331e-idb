from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

#Public IP
# USER ="postgres"
# PASSWORD ="group1"
# PUBLIC_IP_ADDRESS ="35.223.163.34"
# DBNAME ="businessdb"
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')

#Local host
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:zkj1234@localhost:5436/businessdb'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

link_res_to_cuisine = db.Table('link_res_to_cuisine',
   db.Column('res_name', db.String(), db.ForeignKey('restaurant.res_name')),
   db.Column('cuisine_id', db.Integer, db.ForeignKey('cuisine.id'))
   )

link_drink_to_cuisine = db.Table('link_drink_to_cuisine',
   db.Column('drink_name', db.String(), db.ForeignKey('drink_place.drink_name')),
   db.Column('cuisine_id', db.Integer, db.ForeignKey('cuisine.id'))
   )

link_res_to_drink = db.Table('link_res_to_drink',
   db.Column('res_name', db.String(), db.ForeignKey('restaurant.res_name')),
   db.Column('drink_name', db.String(), db.ForeignKey('drink_place.drink_name'))
   )


class Restaurant(db.Model):
    __tablename__ = 'restaurant'

    business_id = db.Column(db.String())
    res_name = db.Column(db.String(), primary_key=True)
    phone = db.Column(db.String())
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    address = db.Column(db.String())
    rating = db.Column(db.String())
    hours = db.Column(db.PickleType)
    desc = db.Column(db.String())
    reveiws = db.Column(db.String())
    photos = db.Column(db.PickleType)
    cuisine = db.Column(db.PickleType)
    price_level = db.Column(db.String())
    delivery = db.Column(db.Boolean)

    nearby_drinks = db.relationship('Drink_Place', secondary='link_res_to_drink', backref='nearby_res')
    #nearby_drinks = db.Column(db.PickleType)
    #cuisine_type = db.Column(db.PickleType)

class Drink_Place(db.Model):
    __tablename__ = 'drink_place'

    business_id = db.Column(db.String())
    drink_name = db.Column(db.String(), primary_key=True)
    phone = db.Column(db.String())
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    address = db.Column(db.String())
    rating = db.Column(db.String, nullable=True)
    hours = db.Column(db.PickleType)
    desc = db.Column(db.String())
    reveiws = db.Column(db.String())
    photos = db.Column(db.PickleType)
    cuisine = db.Column(db.PickleType)
    price_level = db.Column(db.String())
    delivery = db.Column(db.Boolean)

    #cuisine_type = db.relationship('Cuisine', secondary='link_drink_to_cuisine', backref='drink_places')
    #nearby_res = db.Column(db.PickleType)
    #cuisine_type = db.Column(db.PickleType)


class Cuisine(db.Model):
    """
    Cuisine class has 3 attrbiutes (for now)
    id (index)
    name
    type
    """
    __tablename__ = 'cuisine'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80), nullable = False)
    type = db.Column(db.String(80), nullable = False)

    #restaurants = db.relationship('Restaurant', secondary='link_res_to_cuisine', backref='cuisine_type')
    restaurants = db.Column(db.PickleType)
    drink_places = db.Column(db.PickleType)

#with app.app_context():
    #db.drop_all()
    #db.create_all()