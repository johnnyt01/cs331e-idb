FILES :=                             \
    models.html                      \
    models.py                      	 \
	IDB3.log 						 \

ifeq ($(shell uname), Darwin)
	PYDOC    := pydoc3

endif

models.html: models.py
	$(PYDOC) -w models

IDB3.log:
	git log > IDB3.log