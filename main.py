from flask import Flask, render_template, request, redirect, url_for, jsonify, json, Response, make_response, json
from create_db import app, db, Restaurant, Drink_Place, Cuisine, create_restaurant
import os
import requests
from flask_sqlalchemy import SQLAlchemy

#app = Flask(__name__)

@app.route('/')
def index():
    return render_template("splash.html")

@app.route('/about')
def aboutPage():
    api_url = 'https://gitlab.com/api/v4/'
    project_id = 42859168
    access_token = 'glpat-KJMzs7Qf81fMzYcVQqxu'
    page = 1
    per_page = 20
    url = f'{api_url}projects/{project_id}/repository/commits'
    headers = {'Authorization': f'Bearer {access_token}'}
    params = {'page': page, 'per_page': per_page} # Specify pagination parameters
    commits = []
    commit_counts = {}  
    total_issues = 0
    issue_counts = {}
    issues_per_contributor = {}
    issues_per_contributor_list = []
    bryan_johnny_CC={'bryan': 0, 'johnny':0}

    #COMMITS-------------------------------------------------------
    
    while True:
        url = f'{api_url}projects/{project_id}/repository/commits'

        response = requests.get(url, headers=headers, params=params)

        if response.status_code == 200:
            commits_page = response.json()
            commits.extend(commits_page)
            if len(commits_page) < per_page:
                break
            else:
                params['page'] += 1
        else:
            print('Failed to fetch commits from GitLab API.')
            break
    tot_num_commits = len(commits)
    for commit in commits:
        author_name = commit['author_name']
        if author_name in commit_counts:
            commit_counts[author_name] += 1
        else:
            commit_counts[author_name] = 1
    for key in commit_counts:
        if key == "BryanDoan" or key =="Bryan Doan":
            bryan_johnny_CC["bryan"]= commit_counts[key]+bryan_johnny_CC["bryan"]
        elif key =="Johnny Tran" or key == "johnnyt01":
            bryan_johnny_CC['johnny']= commit_counts[key]+ bryan_johnny_CC['johnny']
    # for contributor, count in commit_counts.items():
    #     print(f'{contributor}: {count} commits')

    #ISSUES-----------------------------------------------------    
    

# Loop through paginated API requests to get all issues
    while True:
        # Make API request to get issues for current page
        url = f'{api_url}projects/{project_id}/issues'
        params = {'page': page, 'per_page': per_page}
        headers = {'Private-Token': access_token}
        response = requests.get(url, headers=headers, params=params)

        # Check for successful API response
        if response.status_code == 200:
            # Extract the list of issues from the response
            issues = response.json()

            # Get the total number of issues for the current page
            num_issues = len(issues)

            # Add the number of issues for the current page to the total
            total_issues += num_issues

            # If the number of issues for the current page is less than the per_page limit,
            # we have reached the end of the pagination, break out of the loop
            if num_issues < per_page:
                break

            # Increment the page number for the next API request
            page += 1
        else:
            print(f'Error: {response.status_code} - {response.text}')
            break
# #ISSUES PER
    page = 1
    per_page = 20
    params = {'page': page, 'per_page': 100}
    while True:
        # Make API request to get issues for the current page
        url = f'{api_url}projects/{project_id}/issues'
        response = requests.get(url, headers=headers, params=params)

        # Check for successful API response
        if response.status_code == 200:
            # Extract the list of issues from the response
            issues = response.json()
            # If there are no more issues, break out of the loop
            if not issues:
                break

            # Loop through the issues and count the number of issues per contributor
            for issue in issues:
                # Extract the author information from the issue
                author_id = issue['author']['id']
                author_name = issue['author']['name']

                # If the author is not already in the dictionary, add them with a count of 1
                if author_id not in issues_per_contributor:
                    issues_per_contributor[author_id] = {'name': author_name, 'count': 1}
                # If the author is already in the dictionary, increment their count by 1
                else:
                    issues_per_contributor[author_id]['count'] += 1

            # Increment the page number for the next API request
            params['page'] += 1
       
# Loop through the dictionary to print the number of issues per contributor
    print('Issues per contributor:')
    for author_id, info in issues_per_contributor.items():
        print(f'{info["name"]}: {info["count"]}')
        issues_per_contributor_list.append(info["count"])
        print(info["count"])
    print(issues_per_contributor_list)
    print(bryan_johnny_CC)
    return render_template("about.html", tot_num_commits = tot_num_commits, bryan_johnny_CC = bryan_johnny_CC, commit_counts = commit_counts, total_issues = total_issues, issues_per_contributor_list = issues_per_contributor_list)



@app.route('/cuisines')
def cuisine_model():
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', "name", type=str)
    if sort_by == "id":
        all_cuisines = db.session.query(Cuisine).order_by(Cuisine.id).all()
    elif sort_by == "name":
        all_cuisines = db.session.query(Cuisine).order_by(Cuisine.name).all()
    elif sort_by == "type":
        all_cuisines = db.session.query(Cuisine).order_by(Cuisine.type).all()
    #all_cuisines = db.session.query(Cuisine).all()
    all_drink_places = db.session.query(Drink_Place).all()
    all_restaurants = db.session.query(Restaurant).all()
    restaurant_list=[]
    drink_list=[]
    amount_of_res=[]
    amount_of_drinks=[]
    for c in all_cuisines:
        for i in all_restaurants:
            if c.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in i.cuisine):
                restaurant_list.append(i)
        amount_of_res.append(len(restaurant_list))
        restaurant_list.clear()
    
        for j in all_drink_places:
            if c.name.upper() in (k.upper().replace(" RESTAURANT", '') for k in j.cuisine):
                drink_list.append(j)
        amount_of_drinks.append(len(drink_list))
        drink_list.clear()
        
    
    if sort_by == "id":
        cuisines = db.session.query(Cuisine).order_by(Cuisine.id).paginate(page=page, per_page=10)
    elif sort_by == "name":
        cuisines = db.session.query(Cuisine).order_by(Cuisine.name).paginate(page=page, per_page=10)
    elif sort_by == "type":
        cuisines = db.session.query(Cuisine).order_by(Cuisine.type).paginate(page=page, per_page=10)

    return render_template("cuisine_models.html", cuisines=cuisines, amount_of_res=amount_of_res, amount_of_drinks=amount_of_drinks, sort_by=sort_by)

@app.route('/cuisine/<id>')
def cuisine_page(id):
    cuisine_name = db.session.query(Cuisine).get(id)
    all_drink_places = db.session.query(Drink_Place).all()
    all_restaurants = db.session.query(Restaurant).all()
    restaurant_list=[]
    drink_list=[]

    for i in all_restaurants:
        if cuisine_name.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in i.cuisine):
            restaurant_list.append(i)
    
    for j in all_drink_places:
        if cuisine_name.name.upper() in (k.upper().replace(" RESTAURANT", '') for k in j.cuisine):
            drink_list.append(j)

    return render_template("cuisine_page.html", type=cuisine_name, restaurant_list=restaurant_list, drink_list=drink_list)

@app.route('/drinks')
def drinks_model():
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', "name", type=str)
    #drink_places = db.session.query(Drink_Place).paginate(page=page, per_page=10)
    if sort_by == "rating":
        drink_places = db.session.query(Drink_Place).order_by(Drink_Place.rating).paginate(page=page, per_page=10)
    elif sort_by == "price":
        drink_places = db.session.query(Drink_Place).order_by(Drink_Place.price_level).paginate(page=page, per_page=10)
    elif sort_by == "delivery":
        drink_places = db.session.query(Drink_Place).order_by(Drink_Place.delivery).paginate(page=page, per_page=10)
    else:
        drink_places = db.session.query(Drink_Place).order_by(Drink_Place.drink_name).paginate(page=page, per_page=10)
    
    #drink_places = db.session.query(Drink_Place).all()
    
    return render_template("drinks_models.html", drinks=drink_places, sort_by=sort_by)

@app.route('/drinks/<drink_name>')
def drink_page(drink_name):
    drink_place = db.session.query(Drink_Place).get(drink_name)
    all_cuisines = db.session.query(Cuisine).all()
    cuisine_list =[]

    for i in all_cuisines:
        if i.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in drink_place.cuisine):
            cuisine_list.append(i)

    return render_template("drink_page.html", drink = drink_place, cuisine_list=cuisine_list)

@app.route('/restaurants')
def restaurant_model():
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by', "name", type=str)
    #restaurant = db.session.query(Restaurant).paginate(page=1, per_page=10)
    if sort_by == "rating":
        restaurant = db.session.query(Restaurant).order_by(Restaurant.rating).paginate(page=page, per_page=10)
    elif sort_by == "price":
        restaurant = db.session.query(Restaurant).order_by(Restaurant.price_level).paginate(page=page, per_page=10)
    elif sort_by == "delivery":
        restaurant = db.session.query(Restaurant).order_by(Restaurant.delivery).paginate(page=page, per_page=10)
    else:
        restaurant = db.session.query(Restaurant).order_by(Restaurant.res_name).paginate(page=page, per_page=10)


    
    #restaurant = db.session.query(Restaurant).all()

    return render_template('restaurants_models.html', restaurants=restaurant, sort_by=sort_by)

@app.route('/restaurant/<res_name>')
def restaurant_page(res_name):
    estab = db.session.query(Restaurant).get(res_name)
    if not estab.rating:
        estab.rating = "Not Available"
    else:
        estab.rating = estab.rating[:3]
    all_cuisines = db.session.query(Cuisine).all()
    cuisine_list =[]

    for i in all_cuisines:
        if i.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in estab.cuisine):
            cuisine_list.append(i)
    return render_template("restaurant_page.html", restaurant=estab, cuisine_list=cuisine_list)

@app.route('/test_model')
def test_model():

	restaurant = db.session.query(Restaurant).all()
	return render_template('test_model.html', restaurants=restaurant)

@app.route('/search')
def search():
    query = request.args.get('query', '')
    results = {'restaurants': [], 'drinks': [], 'cuisines': []}
    if query:
        # Perform search query here and populate results dictionary
        results['restaurants'] = db.session.query(Restaurant).filter(Restaurant.res_name.ilike('%'+query+'%')).all()
        results['drinks'] = db.session.query(Drink_Place).filter(Drink_Place.drink_name.ilike('%'+query+'%')).all()
        results['cuisines'] = db.session.query(Cuisine).filter(Cuisine.name.ilike('%'+query+'%')).all()
    
    return render_template('search_results.html', query=query, results=results)

# API CALL: getting a list of all restaurants 
@app.route('/allRestuarants')
def all_restauarnts():
    all_rest = db.session.query(Restaurant).all()

    response = list()

    for rest in all_rest:
        response.append({

            "business_id": rest.business_id,
            "name": rest.res_name,
            "phone": rest.phone,
            "latitude": rest.latitude,
            "longitude": rest.longitude,
            "address": rest.address,
            "rating": rest.rating,
            "hours": rest.hours,
            "desc": rest.desc,
            "reviews": rest.reveiws,
            "photos": rest.photos,
            "cuisine": rest.cuisine,
            "price_level": rest.price_level,
            "delivery": rest.delivery
        })

    return make_response({ 
        'restaurants': response
    }, 200) 

# API CALL: getting a list of all cuisines 
@app.route('/allDrinks')
def all_drinks():
    all_drinks = db.session.query(Drink_Place).all()

    response = list()

    for drink in all_drinks:
        response.append({

            "business_id": drink.business_id,
            "name": drink.drink_name,
            "phone": drink.phone,
            "latitude": drink.latitude,
            "longitude": drink.longitude,
            "address": drink.address,
            "rating": drink.rating,
            "hours": drink.hours,
            "desc": drink.desc,
            "reviews":  drink.reveiws,
            "photos":  drink.photos,
            "cuisine":  drink.cuisine,
            "price_level": drink.price_level,
            "delivery": drink.delivery
        })

    return make_response({ 
        'drink places': response
    }, 200) 

@app.route('/allCuisines', methods=['GET', 'POST'])
def all_cuisines():
    all_cuisines = db.session.query(Cuisine).all()
    
    response = list()

    for cuisine in all_cuisines:
        response.append({
            "id": cuisine.id,
            "name": cuisine.name,
            "type": cuisine.type
        })

    return make_response({ 
        'cuisines': response
    }, 200) 
        
#API CALL: adding   
@app.route('/ResInfo/<res_name>/', methods=['GET','POST'])
def findRestaurant(res_name):
    #rest = db.session.query(Restaurant).get(res_name)
    rest = Restaurant.query.filter_by(res_name = res_name).first()

    if rest:
        response = {
        "business_id": rest.business_id,
        "name": rest.res_name,
        "phone": rest.phone,
        "latitude": rest.latitude,
        "longitude": rest.longitude,
        "address": rest.address,
        "rating": rest.rating,
        "hours": rest.hours,
        "desc": rest.desc,
        "reviews": rest.reveiws,
        "photos": rest.photos,
        "cuisine": rest.cuisine,
        "price_level": rest.price_level,
        "delivery": rest.delivery
        }
    return make_response(response, 200)

@app.route('/CuisineInfo/<int:cuisine_id>/', methods=['GET','POST'])
def findCuisine(cuisine_id):
    cuisine = Cuisine.query.filter_by(id = cuisine_id).first()
    # db.session.query(Cuisine).get(int(cuisine_id))
    if cuisine:
        response = {
            "id": cuisine.id,
            "name": cuisine.name,
            "type": cuisine.type
        }

    return make_response(response, 200)

@app.route('/DrinkInfo/<drink_id>/', methods=['GET','POST'])
def findDrink(drink_id):
    drink = Drink_Place.query.filter_by(drink_name = str(drink_id)).first()
    if drink:
        response = {
            "business_id": drink.business_id,
            "name": drink.drink_name,
            "phone": drink.phone,
            "latitude": drink.latitude,
            "longitude": drink.longitude,
            "address": drink.address,
            "rating": drink.rating,
            "hours": drink.hours,
            "desc": drink.desc,
            "reviews":  drink.reveiws,
            "photos":  drink.photos,
            "cuisine":  drink.cuisine,
            "price_level": drink.price_level,
            "delivery": drink.delivery
        }

    return make_response(response, 200)

@app.route('/cuisine/add', methods=['GET', 'POST'])
def addCuisine():
    if request.method == 'POST':
        data = request.json
        cuis_id = data['id']
        cuis_name = data['name']
        cuis_type = data['type']

        
        cuis = Cuisine.query.filter_by(id = cuis_id).first()

        if not cuis:
            try:
                cuis = Cuisine( 
                    id = cuis_id,
                    name = cuis_name,
                    type = cuis_type
                ) 

                db.session.add(cuis) 
                db.session.commit()
                
                responseObject = { 
                        'status' : 'success'
                    }            
                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
        else:
            responseObject = { 
                'status' : 'fail', 
                'message': 'User already exists'
            } 

            return make_response(responseObject, 403)            

@app.route('/restaurant/add', methods=['GET', 'POST'])
def addRestaurant():
    if request.method == 'POST':
        data = request.json
        rest_id = data['business_id']
        rest_name= data['res_name']
        rest_phone = data['phone']
        rest_lat = data['latitude']
        rest_long = data['longitude']
        rest_addy = data['address']
        rest_rating = data['rating']
        rest_hours = data['hours']
        rest_desc = data['desc']
        rest_reviews = data['reveiws']
        rest_photos = data['photos']
        rest_cuis = data['cuisine']
        rest_price = data['price_level']
        rest_delivery = data['delivery']
        
        restaurant = Restaurant.query.filter_by(res_name = rest_name).first()

        if not restaurant:
            try:
                restaurant = Restaurant(
                    business_id = rest_id,
                    res_name = rest_name,
                    phone = rest_phone,
                    latitude = rest_lat,
                    longitude = rest_long,
                    address = rest_addy,
                    rating = rest_rating,
                    hours = rest_hours,
                    desc = rest_desc,
                    reveiws = rest_reviews,
                    photos = rest_photos,
                    cuisine = rest_cuis,
                    price_level = rest_price,
                    delivery = rest_delivery
                ) 

                db.session.add(restaurant) 
                db.session.commit()
                
                responseObject = { 
                        'status' : 'success'
                    }            
                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
        else:
            responseObject = { 
                'status' : 'fail', 
                'message': 'User already exists'
            } 

            return make_response(responseObject, 403) 
        
@app.route('/drink/add', methods=['GET', 'POST'])
def addDrink():
    if request.method == 'POST':
        data = request.json
        drink_id = data['business_id']
        drink_name= data['drink_name']
        drink_phone = data['phone']
        drink_lat = data['latitude']
        drink_long = data['longitude']
        drink_addy = data['address']
        drink_rating = data['rating']
        drink_hours = data['hours']
        drink_desc = data['desc']
        drink_reviews = data['reveiws']
        drink_photos = data['photos']
        drink_cuis = data['cuisine']
        drink_price = data['price_level']
        drink_delivery = data['delivery']
        
        drink = Drink_Place.query.filter_by(drink_name = drink_name).first()

        if not drink:
            try:
                drink = Drink_Place(
                    business_id = drink_id,
                    drink_name = drink_name,
                    phone = drink_phone,
                    latitude = drink_lat,
                    longitude = drink_long,
                    address = drink_addy,
                    rating = drink_rating,
                    hours = drink_hours,
                    desc = drink_desc,
                    reveiws = drink_reviews,
                    photos = drink_photos,
                    cuisine = drink_cuis,
                    price_level = drink_price,
                    delivery = drink_delivery
                ) 
                
                db.session.add(drink) 
                db.session.commit()
                
                responseObject = { 
                        'status' : 'success'
                    }            
                return make_response(responseObject, 200) 
            except: 
                responseObject = { 
                    'status' : 'fail'
                } 

                return make_response(responseObject, 400) 
        else:
            responseObject = { 
                'status' : 'fail', 
                'message': 'User already exists'
            } 

            return make_response(responseObject, 403) 
        
@app.route('/<table>/<id>/delete/', methods =['GET', 'POST']) 
def delete(table, id):

    # checking instance
    if table == "restaurant":
        obj = Restaurant.query.filter_by(res_name = str(id)).first() 
    if table == "drink":
        #table = "Drink_Place"
        obj = Drink_Place.query.filter_by(drink_name = str(id)).first()
    if table == "cuisine": 
        obj = Cuisine.query.filter_by(id = int(id)).first()
    
    if obj: 
        try: 
            db.session.delete(obj) 
            db.session.commit() 
            # response 
            responseObject = { 
                'status' : 'success'
            } 

            return make_response(responseObject, 200) 
        except: 
            responseObject = { 
                'status' : 'fail'
            } 

            return make_response(responseObject, 400) 
        
    else: 
        # if user already exists then send status as fail 
        responseObject = { 
            'status' : 'fail', 
            'message': 'User does not exist'
        } 

        return make_response(responseObject, 403)    

@app.route('/search/restaurant')
def search_rest():
    query = request.args.get('query', '')
    results = {'restaurants': [], 'drinks': [], 'cuisines': []}
    if query:
        # Perform search query here and populate results dictionary
        results['restaurants'] = db.session.query(Restaurant).filter(Restaurant.res_name.ilike('%'+query+'%')).all()
    
    return render_template('search_results.html', query=query, results=results)

@app.route('/search/cuisine')
def search_cuis():
    query = request.args.get('query', '')
    results = {'restaurants': [], 'drinks': [], 'cuisines': []}
    if query:
        # Perform search query here and populate results dictionary
        results['cuisines'] = db.session.query(Cuisine).filter(Cuisine.name.ilike('%'+query+'%')).all()
    
    return render_template('search_results.html', query=query, results=results)

@app.route('/search/drinks')
def search_drink():
    query = request.args.get('query', '')
    results = {'restaurants': [], 'drinks': [], 'cuisines': []}
    if query:
        # Perform search query here and populate results dictionary
        results['drinks'] = db.session.query(Drink_Place).filter(Drink_Place.drink_name.ilike('%'+query+'%')).all()
    
    return render_template('search_results.html', query=query, results=results)
        

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 2023)
