import json
from models import app, db, Restaurant, Drink_Place, Cuisine
from math import sin, cos, sqrt, atan2, radians
import os

def load_json(filename):
    file_path = "/Users/yuzi/Desktop/CS331E_new/proj/cs331e-idb/" + filename
    with open(file_path) as file:
        jsn = json.load(file)
    return jsn



def create_restaurant():

    dict = load_json("businesses.json")
    duplicate_check = []
    drink_cuisines = ["Cafe", "Bar", "Drinks", "Coffee And Tea", "Bar & grill", "Cocktail bar", "Sports bar", "Wine bar", 'Bar and Grill', 'Smoothies And Juices', 'Smoothies & Juices', 'Beverages', 'Alcohol']
    for business in dict['Businesses']:

        # Check if \ is in each entity and remove it because it interferes with json.loads
        if "\\" in business:
            business = business.replace("\\", '')
        business = json.loads(business)

        # Set all parameters
        business_id = business['business_id']
        name = business['name']
        phone = str(business['phone'])
        latitude = business['latitude']
        longitude = business['longitude']
        address = business['address']
        rating = business['rating']
        hours = business['hours']
        hours = fix_hours(hours)
        desc = business['desc']
        reveiws = "https://www.yelp.com/search?find_desc=" + name.replace(' ','').replace('\'', '').replace("Restaurant", '') +"&find_loc=Austin%2C+TX"
        photos = business['photos']
        cuisine = business['cuisine']
        price_level = business['price_level']
        delivery = business['delivery']

        if isinstance(photos[0], list):
            if len(photos[0]) == 0:
                photos = "None"
            else:
                photos = photos[0][0]
        else:
            photos = photos[0]['photo_url']

        # Checks for duplicates and is considered a restaurant if no
        # drink_cuisines are listed in its own cuisine
        # Fill in restaurant table
        if name not in duplicate_check and not any(item in cuisine for item in drink_cuisines):
            newRestaurant = Restaurant(business_id=business_id, res_name=name,
                                       phone=phone, latitude=latitude, longitude=longitude,
                                       address=address, rating=rating, hours=hours, desc=desc,
                                       reveiws=reveiws, photos=photos, cuisine=cuisine,
                                       price_level=price_level, delivery=delivery)#,nearby_drinks=[], cuisine_type=[])
            db.session.add(newRestaurant)
            #print(newRestaurant.nearby_drinks)
            db.session.commit()
            #print(newRestaurant.cuisine)
            duplicate_check.append(name)
            #print(cuisine)


def create_drinks():

    dict = load_json("businesses.json")
    duplicate_check = []
    drink_cuisines = ["Cafe", "Bar", "Drinks", "Coffee And Tea", "Bar & grill", "Cocktail bar", "Sports bar", "Wine bar", 'Bar and Grill', 'Smoothies And Juices', 'Smoothies & Juices', 'Beverages', 'Alcohol']
    for business in dict['Businesses']:

        # Check if \ is in each entity and remove it because it interferes with json.loads
        if "\\" in business:
            business = business.replace("\\", '')
        business = json.loads(business)

        # Set all parameters
        business_id = business['business_id']
        name = business['name']
        phone = str(business['phone'])
        latitude = business['latitude']
        longitude = business['longitude']
        address = business['address']
        rating = business['rating']
        hours = business['hours']
        hours = fix_hours(hours)
        desc = business['desc']
        reveiws = "https://www.yelp.com/search?find_desc=" + name.replace(' ','').replace('\'', '').replace("Restaurant", '') +"&find_loc=Austin%2C+TX"
        photos = business['photos']
        cuisine = business['cuisine']
        price_level = business['price_level']
        delivery = business['delivery']

        if isinstance(photos[0], list):
            if len(photos[0]) == 0:
                photos = "None"
            else:
                photos = photos[0][0]
        else:
            photos = photos[0]['photo_url']


        # Fill in drink_place table
        if name not in duplicate_check and any(item in cuisine for item in drink_cuisines):
            newDrinkPlace = Drink_Place(business_id=business_id, drink_name=name,
                                        phone=phone, latitude=latitude, longitude=longitude,
                                        address=address, rating=rating, hours=hours, desc=desc,
                                        reveiws=reveiws, photos=photos, cuisine=cuisine,
                                        price_level=price_level, delivery=delivery)#, nearby_res=[], cuisine_type=[])
            db.session.add(newDrinkPlace)
            #print(newDrinkPlace.nearby_res)
            db.session.commit()

            rest_list = db.session.query(Restaurant).filter(Restaurant.longitude < longitude + 0.005).filter(Restaurant.longitude > longitude - 0.005).filter(Restaurant.latitude < latitude + 0.005).filter(Restaurant.latitude > latitude - 0.005).all()
            """for i in rest_list:
                if nearby(longitude, latitude, i.longitude, i.latitude):
                    newDrinkPlace.nearby_res.append(i)
                    db.session.commit()"""
            for i in rest_list:
                newDrinkPlace.nearby_res.append(i)
                db.session.commit()
                #print(i)

            duplicate_check.append(name)


def create_cuisine():
    dict = load_json("cuisine_data.json")
    for cuisine in dict["Cuisine"]:

        id = cuisine['index']
        name = cuisine['name']
        type = cuisine['type']
        newCuisine = Cuisine(id=id, name=name, type=type)#, restaurants=[], drink_places=[])
        db.session.add(newCuisine)
        db.session.commit()

        rest_list = db.session.query(Restaurant).all()
        drink_list = db.session.query(Drink_Place).all()
        """for i in rest_list:
            if newCuisine.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in i.cuisine):
                newCuisine.restaurants.append(i)
                db.session.commit()
        #print(db.session.query(Cuisine).all()[0].restaurants)
        #print(db.session.query(Restaurant).all()[0].cuisine_type)

        for i in drink_list:
            if newCuisine.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in i.cuisine):
                newCuisine.drink_places.append(i)
                db.session.commit()"""

        """rest_list = db.session.query(Restaurant).filter(newCuisine.name.upper() in Restaurant.cuisine).all()
        for i in rest_list:
            newCuisine.restaurants.append(i)
            db.session.commit()
        drink_list = db.session.query(Drink_Place).filter(newCuisine.name.upper() in Drink_Place.cuisine).all()
        for i in drink_list:
            newCuisine.drink_places.append(i)
            db.session.commit()"""

def fix_hours(dict_for_hours):
    if dict_for_hours is None:
        return "Not available"
    else:
        list_of_hours =[]
        for i in dict_for_hours:
            if isinstance(dict_for_hours[i], list):
                list_of_hours.append(i +' '+ dict_for_hours[i][0].replace('u202f', '').replace('u2013', ' - '))
            else:
                list_of_hours.append(i +' '+ dict_for_hours[i])
    return list_of_hours

def nearby(x_1,y_1,x_2,y_2):
    R = 6373.0

    lat1 = radians(y_1)
    lon1 = radians(x_1)
    lat2 = radians(y_2)
    lon2 = radians(x_2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance < 1

def create_links():
    rest_list = db.session.query(Restaurant).all()
    drink_list = db.session.query(Drink_Place).all()
    cuisine_list = db.session.query(Cuisine).all()

    for i in drink_list:
        for j in rest_list:
            if nearby(i.longitude, i.latitude, j.longitude, j.latitude):
                i.nearby_res.append(j)
                j.nearby_drinks.append(i)
                db.session.commit()

    for k in cuisine_list:
        for i in rest_list:
            if k.name.upper() in (j.upper().replace(" RESTAURANT", '') for j in i.cuisine):
                k.restaurants.append(i)
                i.cuisine_type.append(k)
                db.session.commit()

                #print(db.session.query(Restaurant).all()[0].cuisine_type)
                #print(j.cuisine_type[0].name)
                #print(i.restaurants[0].res_name)"""
    




#with app.app_context():
    #create_restaurant()
    #create_drinks()
    #create_cuisine()
    #create_links()